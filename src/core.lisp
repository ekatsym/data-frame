(defpackage data-frame.core
  (:nicknames :df.core)
  (:use :cl)
  (:import-from :df
                #:df
                #:get-df
                #:print-df
                #:head-df
                #:add-new-row
                #:add-new-row!
                #:add-new-col
                #:add-new-col!
                #:map-cols
                )
  (:import-from :df.util
                #:append1
                #:while
                ))
(in-package :df.core)


;;; df
(defstruct df
  (ncol
    0
    :type fixnum)
  (nrow
    0
    :type fixnum)
  (headers
    '()
    :type list)
  (table
    (make-hash-table)
    :type hash-table))

(defun get-df (df header)
  (values (gethash header (df-table df))))

(defun set-df (df header list)
  (unless (member header (df-headers df))
    (incf (df-ncol df))
    (setf (df-headers df)
          (append1 (df-headers df) header)))
  (setf (df-nrow df)
        (max (df-nrow df)
             (length list)))
  (setf (gethash header (df-table df))
        list))

(defsetf get-df set-df)

(defun df (&rest header-and-data)
  (let ((df (make-df))
        (hd header-and-data))
    (while hd
      (destructuring-bind (header data . rest-data) hd
        (setf (get-df df header) data
              hd rest-data)))
    df))

(defun print-df (df &optional (stream *standard-output*))
  (format stream "~&|DF ~{|~8s~}|~%"
          (df-headers df))
  (dotimes (i (df-nrow df))
    (format stream "|~3d~{|~8@s~}|~%"
            i
            (mapcar (lambda (header)
                      (nth i (get-df df header)))
                    (df-headers df))))
  df)

(defun head-df (df &optional (n 5) (stream *standard-output*))
  (format stream "~&|DF~{|~8s~}|~%"
          (df-headers df))
  (dotimes (i (min n (df-nrow df)))
    (format stream "|~2d~{|~8@s~}|~%"
            i
            (mapcar (lambda (header)
                      (nth i (get-df df header)))
                    (df-headers df))))
  df)

(defun add-new-row! (df row)
  (mapc (lambda (header x)
          (setf (get-df df header)
                (append1 (get-df df header) x)))
        (df-headers df) row)
  df)

(defun add-new-row (df row)
  (add-new-row! (copy-df df) row))

(defun add-new-col! (df header data)
  (setf (get-df df header) data)
  df)

(defun add-new-col (df header data)
  (add-new-col! (copy-df df) header data))

(defun map-cols (function df)
  (let ((new (df)))
    (mapc (lambda (header)
            (setf (get-df new header)
                  (list (funcall function (get-df df header)))))
          (df-headers df))
    new))

(defpackage df.util
  (:use :cl)
  (:export
    ;; type
    #:positive #:negative #:non-positive #:non-negative #:index
    #:proper-list #:dotted-list #:associative-list

    ;; mathmatics
    #:square #:cube
    #:sum #:maximum #:minimum #:median #:first-quartile #:third-quartile
    #:mean #:variance #:standard-deviation #:unbiased-variance #:standard-error
    #:covariance #:correlation-coefficient

    ;; cons constructor
    #:xcons #:iota #:circular-list

    ;; list convertor
    #:proper-list->dotted-list #:dotted-list->proper-list

    ;; list manipulator
    #:last1 #:take #:drop #:single? #:n-list? #:append1 #:mklist
    #:longer #:filter #:group #:flatten #:prune #:shuffle
    #:find2 #:before #:after #:duplicate
    #:split2 #:splitn #:split #:split2-if #:splitn-if #:split-if
    #:most #:best #:mostn
    #:mapa-b #:map0-n #:map1-n #:mappend #:mapcars #:rmapcar

    ;; ultiple dimensions array
    #:map-2darray #:accumulate-2darray

    ;; simple linear argebra

    ;; string
    #:mkstr

    ;; symbol generetor
    #:symbolicate

    ;; io utility
    #:read-list
    #:read-stream
    #:read-file

    ;; memoize
    #:memoize #:mv-memoize
    #:defmemo #:define-mv-memo

    ;; funciton manipulator
    #:compose #:mv-compose
    #:partial #:rpartial
    #:fn-if #:fn-and #:fn-or
    #:recur

    ;; macro utility
    #:with-gensyms #:once-only
    #:let1 #:when-bind #:when-bind* #:condlet
    #:in #:inq #:in-if #:>case
    #:nif #:while #:till #:for
    #:mvsetq #:mvdo #:mvdo-rebind-gen #:mvdo-gen

    ;; defmacro!
    #:defmacro/g! #:defmacro!

    ;; named macro
    #:named-lambda #:named-let

    ;; destructuring macro
    #:dlambda #:ddefun

    ;; anaphoric macro
    #:it #:aif #:awhen #:awhile #:aand #:acond
    #:self #:alambda
    #:this #:alet

    ;; closure
    ; plane closure
    #:closure #:meta-closure
    #:defclosure #:meta-defclosure
    #:define-meta-closure #:define-meta-defclosure

    ; open-closure
    #:open-closure #:meta-open-closure
    #:define-open-closure #:meta-define-open-closure
    #:define-meta-open-closure #:define-meta-defopen-closure
    #:open-closure-with-initialize
    #:get-open-closure #:with-open-closure

    ;; lazy evaluation
    #:promise #:promise? #:delay #:force

    ;; lazy stream
    #:lcons #:lcar #:lcdr
    #:lnull #:lstream #:lref #:ltake #:ldrop
    #:lreverse #:lappend #:lmember #:lmap

    ;; nondeterministic computation
    #:amb
    #:with-amb

    ;; read macro toggle
    #:sharp-backquote-toggle

    ;; condition
    #:index-error
    #:invalid-let-bindings))
(in-package :df.util)


;;; type
(deftype positive ()
  '(and number (satisfies plusp)))

(deftype negative ()
  '(and number (satisfies minusp)))

(deftype non-positive ()
  '(not positive))

(deftype non-negative ()
  '(not negative))

(deftype index ()
  '(and positive integer))

(defun proper-list? (object)
  (or (null object)
      (and (consp object)
           (proper-list? (cdr object)))))
(deftype proper-list? ()
  '(satisfies proper-list?))

(defun dotted-list? (object)
  (and (consp object)
       (not (null (cdr object)))
       (or (atom object)
           (dotted-list? (cdr object)))))
(deftype dotted-list ()
  '(satisfies dotted-list?))

(defun associative-list? (object)
  (or (atom object)
      (and (consp object)
           (associative-list? (cdr object)))))
(deftype associative-list ()
  '(satisfies associative-list?))

(deftype circular-list ()
  '(not (or proper-list dotted-list)))


;;; mathematics
(defun square (x)
  (* x x))

(defun cube (x)
  (* x x x))


;;; statistics
(defun sum (data)
  (reduce #'+ data))

(defun maximum (data)
  (reduce #'max data))

(defun minimum (data)
  (reduce #'min data))

(defun median (data)
  (let ((len (length data))
        (sorted-data (sort (copy-list data) #'<=)))
    (if (oddp len)
        (nth (/ (1- len) 2) sorted-data)
        (* 1/2 (+ (nth (1- (/ len 2)) sorted-data)
                  (nth (/ len 2) sorted-data))))))

(defun first-quartile (data)
  (let ((len (length data))
        (sorted-data (sort (copy-list data) #'<=)))
    (ecase (mod len 4)
      (0 (* 1/2 (+ (nth (1- (/ len 4)) sorted-data)
                   (nth (/ len 4) sorted-data))))
      (1 (* 1/2 (+ (nth (1- (/ (1- len) 4)) sorted-data)
                   (nth (/ (1- len) 4) sorted-data))))
      (2 (nth (/ (1- (/ len 2)) 2) sorted-data))
      (3 (nth (/ (1- (/ (1- len) 2)) 2) sorted-data)))))

(defun third-quartile (data)
  (let ((len (length data))
        (sorted-data (sort (copy-list data) #'>=)))
    (ecase (mod len 4)
      (0 (* 1/2 (+ (nth (1- (/ len 4)) sorted-data)
                   (nth (/ len 4) sorted-data))))
      (1 (* 1/2 (+ (nth (1- (/ (1- len) 4)) sorted-data)
                   (nth (/ (1- len) 4) sorted-data))))
      (2 (nth (/ (1- (/ len 2)) 2) sorted-data))
      (3 (nth (/ (1- (/ (1- len) 2)) 2) sorted-data)))))

(defun mean (data)
  (let ((acc 0)
        (len 0))
    (dolist (x data)
      (incf acc x)
      (incf len))
    (/ acc len)))

(defun variance (data)
  (let ((mean (mean data)))
    (/ (reduce (lambda (acc x)
                 (+ acc (square (- x mean))))
               data)
       (length data))))

(defun standard-deviation (data)
  (sqrt (variance data)))

(defun unbiased-variance (data)
  (let ((mean (mean data)))
    (/ (reduce (lambda (acc x)
                 (+ acc (square (- x mean))))
               data)
       (1- (length data)))))

(defun standard-error (data)
  (sqrt (/ (unbiased-variance data)
           (length data))))

(defun covariance (xy-data)
  (let ((u1 (mean (mapcar #'car xy-data)))
        (u2 (mean (mapcar #'cdr xy-data))))
    (loop :for xy :in xy-data
          :sum (* (- (car xy) u1) (- (cdr xy) u2)))))

(defun correlation-coefficient (xy-data)
  (/ (covariance xy-data)
     (* (standard-deviation (mapcar #'car xy-data))
        (standard-deviation (mapcar #'cdr xy-data)))))


;;; list constructor
(defun xcons (se2 se1)
  (cons se1 se2))

(defun iota (size &key (start 0) (step 1))
  (loop :for i :below size
        :collect (+ start (* step i))))


;;; cons convert
(defun proper-list->dotted-list (proper-list)
  (apply #'list* proper-list))

(defun dotted-list->proper-list (dotted-list)
  (labels ((rec (dlst acc)
             (declare (optimize speed))
             (if (atom dlst)
                 (cons dlst acc)
                 (rec (cdr dlst) (cons (car dlst) acc)))))
    (nreverse (rec dotted-list '()))))


;;; list manipulation
(defun last1 (list)
  (car (last list)))

(defun take (list n)
  (labels ((rec (m lst acc)
             (declare (optimize speed) (type fixnum m))
             (cond ((zerop m)
                    acc)
                   ((null lst)
                    (error 'index-error :context 'take))
                   (t
                    (rec (1- m) (rest lst) (cons (first lst) acc))))))
    (nreverse (rec n list '()))))

(defun drop (list n)
  (declare (optimize speed) (type fixnum n))
  (cond ((zerop n)
         list)
        ((null list)
         (error 'index-error :context 'drop))
        (t
         (drop (cdr list) (1- n)))))

(defun single? (list)
  (and (consp list)
       (null (cdr list))))

(defun n-list? (n list)
  (declare (optimize speed) (type fixnum n))
  (cond ((zerop n)
         (null list))
        ((plusp n)
         (and (consp list)
              (n-list? (1- n) (cdr list))))
        ((minusp n)
         (error 'index-error
                :context 'n-list))))

(defun append1 (list object)
  (append list (list object)))

(defun mklist (object)
  (if (listp object)
      object
      (list object)))

(defun longer (x y)
  (if (and (listp x) (listp y))
      (do ((lstx x (cdr lstx))
           (lsty y (cdr lsty)))
          ((or (endp lstx) (endp lsty))
           (and (not (endp lstx)) (endp lsty))))
      (> (length x) (length y))))

(defun filter (function list)
  (do ((lst list (cdr lst))
       (acc '() (let ((x (funcall function (car lst))))
                  (if x (cons x acc) acc))))
      ((endp lst) (nreverse acc))))

(defun group (source n)
  (when (zerop n) (error "zero length"))
  (labels ((rec (src acc)
             (let ((rest (nthcdr n src)))
               (if (null rest)
                   (cons src acc)
                   (rec rest (cons (subseq src 0 n) acc))))))
    (if source
        (nreverse (rec source '()))
        '())))

(defun flatten (x)
  (labels ((rec (x acc)
             (cond ((null x) acc)
                   ((atom x) (cons x acc))
                   (t (rec (car x) (rec (cdr x) acc))))))
    (rec x '())))

(defun prune (test tree)
  (labels ((rec (tree acc)
             (cond ((null tree)
                    (nreverse acc))
                   ((consp (car tree))
                    (rec (cdr tree)
                         (cons (rec (car tree) '()) acc)))
                   (t (rec (cdr tree)
                           (if (funcall test (car tree))
                               acc
                               (cons (car tree) acc)))))))
    (rec tree '())))

(defun shuffle (x y)
  (cond ((null x) y)
        ((null y) x)
        (t (list* (car x) (car y)
                  (shuffle (cdr x) (cdr y))))))

(defun find2 (function list)
  (declare (optimize speed)
           (type function function))
  (if (endp list)
      nil
      (let* ((head (car list))
             (tail (cdr list))
             (val (funcall function head)))
        (if val
            (values head val)
            (find2 function tail)))))

(defun before (x y list &key (test #'eql))
  (declare (optimize speed)
           (type function test))
  (if (endp list)
      nil
      (destructuring-bind (head &rest tail) list
        (cond ((funcall test y head) nil)
              ((funcall test x head) list)
              (t (before x y tail :test test))))))

(defun after (x y list &key (test #'eql))
  (declare (optimize speed)
           (type function test))
  (if (endp list)
      nil
      (destructuring-bind (head &rest tail) list
        (cond ((funcall test x head) nil)
              ((funcall test y head) list)
              (t (after x y tail :test test))))))

(defun duplicate (object list &key (test #'eql))
  (member object (cdr (member object list :test test)) :test test))

(defun split (item list &key (test #'eql))
  (labels ((rec (x f lst tmp acc)
             (declare (optimize (speed 3))
                      (type function f))
             (cond ((endp lst)
                    (cons tmp acc))
                   ((funcall f x (first lst))
                    (rec x f (rest lst) '() (cons tmp acc)))
                   (t
                    (rec x f (rest lst) (cons (first lst) tmp) acc)))))
    (rec item test (reverse list) '() '())))

(defun split2 (item list &key (test #'eql))
  (labels ((rec (x f lst acc)
             (declare (optimize (speed 3))
                      (type function f))
             (cond ((endp lst)
                    (list (nreverse acc) '()))
                   ((funcall f x (first lst))
                    (list (nreverse acc) (rest lst)))
                   (t
                    (rec x f (rest lst) (cons (first lst) acc))))))
    (rec item test list '())))

(defun splitn (n item list &key (test #'eql))
  (labels ((rec (x f i lst acc)
             (declare (optimize (speed 3))
                      (type function f)
                      (type fixnum i))
             (if (<= i 0)
                 (nreverse (cons lst acc))
                 (destructuring-bind (former latter) (split2 x lst :test f)
                   (rec x f (1- i) latter (cons former acc))))))
    (rec item test (1- n) list '())))

(defun split-if (predicate list)
  (labels ((rec (pred lst tmp acc)
             (declare (optimize (speed 3))
                      (type function pred))
             (cond ((endp lst)
                    (cons tmp acc))
                   ((funcall pred (first lst))
                    (rec pred (rest lst) '() (cons tmp acc)))
                   (t
                    (rec pred (rest lst) (cons (first lst) tmp) acc)))))
    (rec predicate (reverse list) '() '())))

(defun split2-if (predicate list)
  (labels ((rec (pred lst acc)
             (declare (optimize (speed 3))
                      (type function pred))
             (cond ((endp lst)
                    (list (nreverse acc) '()))
                   ((funcall pred (first lst))
                    (list (nreverse acc) (rest lst)))
                   (t
                    (rec pred (rest lst) (cons (first lst) acc))))))
    (rec predicate list '())))

(defun splitn-if (n predicate list)
  (labels ((rec (pred i lst acc)
             (declare (optimize (speed 3))
                      (type function pred)
                      (type fixnum i))
             (if (<= i 0)
                 (nreverse (cons lst acc))
                 (destructuring-bind (former latter) (split2-if pred lst)
                   (rec pred (1- i) latter (cons former acc))))))
    (rec predicate (1- n) list '())))

(defun most (function list)
  (if (endp list)
      (values nil nil)
      (let* ((wins (car list))
             (max (funcall function wins)))
        (dolist (x (cdr list))
          (let ((score (funcall function x)))
            (when (> score max)
              (setf wins x
                    max score)))
          (values wins max)))))

(defun best (function list)
  (if (endp list)
      nil
      (let ((wins (car list)))
        (dolist (x (cdr list))
          (when (funcall function wins)
            (setf wins x)))
        wins)))

(defun mostn (function list)
  (if (endp list)
      (values nil nil)
      (let ((result (list (car list)))
            (max (funcall function (car list))))
        (dolist (x (cdr list))
          (let ((score (funcall function x)))
            (cond ((> score max)
                   (setf max score
                         result (list x)))
                  ((= score max)
                   (push x result))))
          (values (nreverse result) max)))))

(defun mapa-b (function a b &optional (step 1))
  (do ((i a (+ i step))
       (acc '() (cons (funcall function i) acc)))
      ((> i b) (nreverse acc))))

(defun map0-n (function n)
  (mapa-b function 0 n))

(defun map1-n (function n)
  (mapa-b function 1 n))

(defun map-> (function start test-fn succ-fn)
  (do ((i start (funcall succ-fn i))
       (acc '() (cons (funcall function i) acc)))
      ((funcall test-fn i) (nreverse acc))))

(defun mappend (function &rest lists)
  (apply #'append (apply #'mapcar function lists)))

(defun mapcars (function &rest lists)
  (let ((result '()))
    (dolist (lst lists)
      (dolist (x lst)
        (push (funcall function x) result)))
    (nreverse result)))

(defun rmapcar (function list &rest more-lists)
  (labels ((rec (lsts acc)
             (if (some #'atom lsts)
                 acc
                 (rec (mapcar #'rest lsts)
                      (cons (apply function (mapcar #'first lsts)) acc)))))
    (rec (cons list more-lists) '())))


;; simple linear argebla
(defun map-vector (function vector &rest more-vectors)
  (apply #'map 'vector function vector more-vectors))

(defun reduce-mector (function initial-value vector)
  (reduce function vector :initial-value initial-value))

(defun v+ (&rest vs)
  (apply #'map 'vector #'+ vs))

(defun s*v (s v)
  (map 'vector (lambda (x) (* s x)) v))

(defun in-v* (v u)
  (loop :for x :across v
        :for y :across u
        :sum (+ x y)))

(defun vector-norm (v)
  (sqrt (in-v* v v)))

(defun m*v (m v)
  (let ((u (make-array (array-dimension m 0)
                       :initial-element 0)))
    (dotimes (j (array-dimension m 0))
      (let ((vj (aref v j)))
        (dotimes (i (array-dimension m 1))
          (incf (aref u j) (* vj (aref m j i))))))
    u))

(defun map-matrix (function matrix &rest more-matrixs)
  (let ((mat (make-array (array-dimensions matrix))))
    (dotimes (i (array-dimension matrix 0))
      (dotimes (j (array-dimension matrix 1))
        (setf (aref mat i j)
              (apply function
                     (mapcar (lambda (m) (aref m i j))
                             (cons matrix more-matrixs))))))
    mat))

(defun reduce-matrix (function initial-value matrix)
  (let ((acc initial-value))
    (dotimes (i (array-dimension matrix 0))
      (dotimes (j (array-dimension matrix 1))
        (setq acc
              (funcall function (aref matrix i j) acc))))
    acc))

(defun m+ (&rest ms)
  (apply #'map-matrix #'+ ms))

(defun s*m (s m)
  (map-matrix (lambda (x) (* s x)) m))

(defun m* (m1 m2)
  (let ((m (make-array (list (array-dimension m1 0)
                             (array-dimension m2 1))
                       :initial-element 0)))
    (dotimes (i (array-dimension m1 0))
      (dotimes (j (array-dimension m2 1))
        (dotimes (k (array-dimension m1 1))
          (incf (aref m i j)
                (* (aref m1 i k)
                   (aref m2 k j))))))
    m))

(defun matrix-sum-of-products (m1 m2)
  (loop :for i :below (array-dimension m1 0)
        :sum (loop :for j :below (array-dimension m1 1)
                   :sum (* (aref m1 i j)
                           (aref m2 i j)))))

(defun convolute-matrix (matrix filter &optional (edge-value 'id))
  (let ((result (make-array (array-dimensions matrix)))
        (m (array-dimension matrix 0))
        (n (array-dimension matrix 1))
        (o (array-dimension filter 0))
        (p (array-dimension filter 1)))
    (loop
      :for i :to (- m o)
      :do
      (loop
        :for j :to (- n p)
        :do
        (let ((sum-of-squares 0))
          (loop
            :for k :below o
            :do
            (loop
              :for l :below p
              :do
              (incf sum-of-squares (* (aref filter k l)
                                      (aref matrix (+ i k) (+ j l))))))
          (setf (aref result (1+ i) (1+ j))
                sum-of-squares))))
    (flet ((fill-edge-id ()
             (loop
               :for i :below m
               :do
               (loop
                 :for l :below (floor p 2.0)
                 :do
                 (setf (aref result i l) (aref matrix i l)
                       (aref result i (- n l 1)) (aref matrix i (- n l 1)))))
             (loop
               :for j :from (floor p 2.0) :below (- n (floor p 2.0))
               :do
               (loop
                 :for k :below (floor o 2.0)
                 :do
                 (setf (aref result k j) (aref matrix k j)
                       (aref result (- m k 1) j) (aref matrix (- m k 1) j)))))
           (fill-edge (x)
             (loop
               :for i :below m
               :do
               (loop
                 :for l :below (floor p 2.0)
                 :do
                 (setf (aref result i l) x
                       (aref result i (- n l 1)) x)))
             (loop
               :for j :from (floor p 2.0) :below (- n (floor p 2.0))
               :do
               (loop
                 :for k :below (floor o 2.0)
                 :do
                 (setf (aref result k j) x
                       (aref result (- m k 1) j) x)))))
      (if (eq edge-value 'id)
          (fill-edge-id)
          (fill-edge edge-value)))
    result))


;;; string manipulation
(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (arg args) (princ arg s))))


;;; symbol manipulation
(defun symbolicate (&rest args)
  (values (intern (apply #'mkstr args))))

;;; I/O utilities
(defun read-list (&rest args)
  (values
    (read-from-string
      (concatenate 'string "(" (apply #'read-line args) ")"))))

(defun read-stream (stream)
  (let ((newline (string #\newline)))
    (reduce (lambda (x y) (concatenate 'string y newline x))
            (loop :for line := (read-line stream nil nil)
                  :while line
                  :collect line))))

(defun read-file (filespec)
  (with-open-file (s filespec
                     :direction :input
                     :if-does-not-exist :error)
    (let ((seq (make-string (file-length s))))
      (read-sequence seq s)
      (string-right-trim " " seq))))

;;; memo
(defun memoize (function)
  (let ((cache (make-hash-table :test #'equal)))
    (lambda (&rest args)
      (multiple-value-bind (val win) (gethash args cache)
        (if win
            val
            (setf (gethash args cache)
                  (apply function args)))))))

(defun mv-memoize (function)
  (let ((cache (make-hash-table :test #'equal)))
    (lambda (&rest args)
      (multiple-value-bind (val win) (gethash args cache)
        (if win
            (apply #'values val)
            (apply #'values
                   (setf (gethash args cache)
                         (multiple-value-list (apply function args)))))))))

(defmacro defmemo (name args &body body)
  (let ((fn (gensym "FN")))
    `(let ((,fn (memoize (lambda ,args ,@body))))
       (defun ,name ,args
         (funcall ,fn ,@args)))))

(defmacro define-mv-memo (name args &body body)
  (let ((fn (gensym "FN")))
    `(let ((,fn (mv-memoize (lambda ,args ,@body))))
       (defun ,name ,args
         (funcall ,fn ,@args)))))


;;; function manipulation
(defun compose2 (g f)
  (lambda (&rest args)
    (funcall g (apply f args))))

(defun mv-compose2 (g f)
  (lambda (&rest args)
    (multiple-value-call g (apply f args))))

(defun compose (&rest functions)
  (reduce #'compose2 functions
          :initial-value #'identity))

(defun mv-compose (&rest functions)
  (reduce #'mv-compose2 functions
          :initial-value #'values))

(defun partial (function &rest args)
  (lambda (&rest rest-args)
    (apply function (append args rest-args))))

(defun rpartial (function &rest args)
  (lambda (&rest rest-args)
    (apply function (reverse (append args rest-args)))))

(defun fn-if (test then &optional else)
  (lambda (x)
    (if (funcall test x)
        (funcall then x)
        (when else (funcall else x)))))

(defun fn-and (function &rest functions)
  (if (null functions)
      function
      (let ((chain (apply #'fn-and functions)))
        (lambda (x) (and (funcall function x)
                         (funcall chain x))))))

(defun fn-or (function &rest functions)
  (if (null functions)
      function
      (let ((chain (apply #'fn-or functions)))
        (lambda (x) (or (funcall function x)
                        (funcall chain x))))))

(defun recur (base? step-back base-case recur-step)
  (declare (optimize speed)
           (type function base?)
           (type function step-back)
           (type function recur-step))
  (lambda (x)
    (do ((i x (funcall step-back i))
         (acc base-case (funcall recur-step i acc)))
        ((funcall base? i) acc))))


;;; macros
(defmacro with-gensyms (names &body forms)
  `(let ,(mapcar (lambda (name)
                   `(,name (gensym ,(string name))))
                 names)
     ,@forms))

(defmacro once-only (names &body body)
  (let ((gensyms (mapcar (lambda (name) (gensym (string name))) names)))
    `(let ,(mapcar (lambda (gsym name)
                     `(,gsym (gensym ,(string name))))
                   gensyms
                   names)
       `(let (,,@(mapcar (lambda (gsym name) ``(,,gsym ,,name)) gensyms names))
          ,(let (,@(mapcar (lambda (gsym name) `(,name ,gsym)) gensyms names))
             ,@body)))))

(defmacro let1 (binding &body body)
  `(let (,binding)
     ,@body))

(defmacro when-bind ((var expr) &body body)
  `(let ((,var ,expr))
     (when ,var ,@body)))

(defmacro when-bind* (binds &body body)
  (if (null binds)
      `(progn ,@body)
      `(let (,(car binds))
         (if ,(caar binds)
             (when-bind* ,(cdr binds) ,@body)))))

(defun condlet-binds (vars clause)
  (mapcar (lambda (bindform)
            (if (consp bindform)
                (cons (cdr (assoc (car bindform) vars))
                      (cdr bindform))))
          (cdr clause)))

(defun condlet-clause (vars clause bodfn)
  `(,(car clause) (let ,(mapcar #'cdr vars)
                    (let ,(condlet-binds vars clause)
                      (,bodfn ,@(mapcar #'cdr vars))))))

(defmacro condlet (clauses &body body)
  (let ((bodfn (gensym "BODFN"))
        (vars (mapcar (lambda (v) (cons v (gensym)))
                      (remove-duplicates
                        (mapcar #'car (mappend #'cdr clauses))))))
    `(labels ((,bodfn ,(mapcar #'car vars) ,@body))
       (cond ,@(mapcar (lambda (clause)
                         (condlet-clause vars clause bodfn))
                       clauses)))))

(defmacro in (object &rest choices)
  (let ((insym (gensym "INSYM")))
    `(let ((,insym ,object))
       (or ,@(mapcar (lambda (c) `(eql ,insym ,c))
                     choices)))))

(defmacro inq (object &rest args)
  `(in ,object ,@(mapcar (lambda (a) `',a)
                         args)))

(defmacro in-if (pred &rest choices)
  (let ((p (gensym "P")))
    `(let ((,p ,pred))
       (or ,@(mapcar (lambda (c) `(funcall ,p ,c))
                     choices)))))

(defun >casex (g clause)
  (let ((key (car clause))
        (rest (cdr clause)))
    (cond ((consp key) `((in ,g ,@key) ,@rest))
          ((inq key t otherwise) `(t ,@rest))
          (t (error "bad >CASE clause")))))

(defmacro >case (expr &body clauses)
  (let ((g (gensym ">CASE")))
    `(let ((,g ,expr))
       (declare (ignorable ,g))
       (cond ,@(mapcar (lambda (clause) (>casex g clause))
                       clauses)))))

(defmacro nif (test zero pos neg)
  (let ((test-sym (gensym "TEST")))
    `(let ((,test-sym ,test))
       (cond ((zerop ,test-sym) ,zero)
             ((plusp ,test-sym) ,pos)
             ((minusp ,test-sym) ,neg)))))

(defmacro while (test &body body)
  `(do ()
       ((not ,test))
       ,@body))

(defmacro till (test &body body)
  `(do ()
       (,test)
       ,@body))

(defmacro for ((var start stop) &body body)
  (let ((stopsym (gensym "STOP")))
    `(do ((,var ,start (1+ ,var))
          (,stopsym ,stop))
         ((> ,var ,stopsym))
         ,@body)))

(defmacro mvpsetq (&rest args)
  (let* ((pairs (group args 2))
         (syms (mapcar (lambda (p)
                         (mapcar (lambda (x)
                                   (gensym (string x)))
                                 (mklist (car p))))
                       pairs)))
    (labels ((rec (ps ss)
               (if (null ps)
                   `(setq ,@(mapcan (lambda (p s)
                                      (shuffle (mklist (car p)) s))
                                    pairs
                                    syms))
                   (let ((body (rec (cdr ps) (cdr ss))))
                     (let ((var/s (caar ps))
                           (expr (cadar ps)))
                       (if (consp var/s)
                           `(multiple-value-bind ,(car ss) ,expr
                              ,body)
                           `(let ((,@(car ss) ,expr))
                              ,body)))))))
      (rec pairs syms))))

(defmacro mvdo (binds (test &rest result) &body body)
  (let ((label (gensym "LABLE"))
        (temps (mapcar (lambda (b)
                         (if (listp (car b))
                             (mapcar (lambda (x) (gensym (string x)))
                                     (car b))
                             (gensym (string b))))
                       binds)))
    `(let ,(mappend #'mklist temps)
       (mvpsetq ,@(mapcan (lambda (b var) (list b var))
                          (mappend #'mklist (mapcar #'car binds))
                          (mappend #'mklist temps))
                ,label
                (when ,test
                  (return (progn ,@result)))
                ,@body
                (mvpsetq ,@(mapcan (lambda (b)
                                     (when (third b)
                                       (list (car b) (third b))))
                                   binds))
                (go ,label)))))

(defun mvdo-rebind-gen (rebinds)
  (cond ((null rebinds) nil)
        ((< (length (car rebinds)) 3)
         (mvdo-rebind-gen (cdr rebinds)))
        (t (cons (list (if (atom (caar rebinds))
                           'setq
                           'multiple-value-setq)
                       (caar rebinds)
                       (third (car rebinds)))
                 (mvdo-rebind-gen (cdr rebinds))))))

(defun mvdo-gen (varlist rebinds endlist body)
  (if (null varlist)
      (let ((label (gensym "LABEL")))
        `(prog nil
               ,label
               (when ,(car endlist)
                 (return (progn ,@(cdr endlist))))
               ,@body
               ,@(mvdo-rebind-gen rebinds)
               (go ,label)))
      (let ((rec (mvdo-gen (cdr varlist) rebinds endlist body)))
        (let ((var/s (caar varlist))
              (expr (cadar varlist)))
          (if (atom var/s)
              `(let ((,var/s ,expr)) ,rec)
              `(multiple-value-bind ,var/s ,expr ,rec))))))

(defmacro mvdo* (varlist endlist &body body)
  (mvdo-gen varlist varlist endlist body))


;;; another defun
(defmacro define-function (name function)
  `(progn
     (setf (symbol-function ',name)
           ,function)
     ',name))


;;; improved defmacro
(defmacro defmacro/g! (name args &rest body)
  (flet ((flatten-defmacro! (x)
           (labels ((rec (x acc)
                      (cond ((null x)
                             acc)
                            #+sbcl ((sb-impl::comma-p x)
                                    (rec (sb-impl::comma-expr x) acc))
                            ((atom x)
                             (cons x acc))
                            (t
                             (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))

         (g!-symbol? (sym)
           (and (symbolp sym)
                (> (length (symbol-name sym)) 2)
                (string= (symbol-name sym)
                         "G!"
                         :start1 0 :end1 2))))

    (let ((syms (remove-duplicates
                  (remove-if-not #'g!-symbol?
                                 (flatten-defmacro! body)))))
      `(defmacro ,name ,args
         (let , (mapcar
                 (lambda (s)
                   `(,s (gensym ,(subseq
                                   (symbol-name s)
                                   2))))
                 syms)
           ,@body)))))

(defmacro defmacro! (name lambda-list &body body)
  (flet ((flatten-defmacro! (x)
           (labels ((rec (x acc)
                      (cond ((null x)
                             acc)
                            #+sbcl ((sb-impl::comma-p x)
                                    (rec (sb-impl::comma-expr x) acc))
                            ((atom x)
                             (cons x acc))
                            (t
                             (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))

         (o!-symbol? (sym)
           (and (symbolp sym)
                (> (length (symbol-name sym)) 2)
                (string= (symbol-name sym)
                         "O!"
                         :start1 0 :end1 2)))

         (o!-symbol->g!-symbol (sym)
           (intern (concatenate 'string "G!" (subseq (symbol-name sym) 2)))))

    (let* ((os (remove-if-not #'o!-symbol?
                              (flatten-defmacro! lambda-list)))
           (gs (mapcar #'o!-symbol->g!-symbol os)))
      `(defmacro/g! ,name ,lambda-list
         `(let ,(mapcar #'list (list ,@gs) (list ,@os))
            ,(progn ,@body))))))


;;; named macro
(defmacro named-lambda (name args &body body)
  `(labels ((,name ,args ,@body))
     #',name))

(defmacro named-let (name bindings &body body)
  `(funcall (named-lambda ,name ,(mapcar #'first bindings)
              ,@body)
            ,@(mapcar #'second bindings)))


;;; destructuring macro
(defmacro! dlambda (&body key-args-body)
  `(lambda (&rest ,g!args)
     (let ((,g!key (first ,g!args))
           (,g!rest-args (rest ,g!args)))
       (case ,g!key
         ,@(mapcar (lambda (k-a-b)
                     (destructuring-bind (key args &rest body) k-a-b
                       (if (or (eq key 'otherwise)
                               (eq key 't))
                           `(otherwise
                              (apply (lambda ,args ,@body) ,g!args))
                           `(,key
                              (apply (lambda ,args ,@body) ,g!rest-args)))))
                   key-args-body)
         (otherwise (error 'dlambda-key-not-found :key ,g!key))))))

(define-condition dlambda-key-not-found (error)
  ((key :initarg :key
        :reader dlambda-key-not-found-key))
  (:report
    (lambda (o s)
      (format s "The key~%~6t~s~%is not found."
              (dlambda-key-not-found-key o)))))

(defmacro! ddefun (name &body key-args-body)
  `(defun ,name (&rest ,g!args)
     (let ((,g!key (first ,g!args))
           (,g!rest-args (rest ,g!args)))
       (case ,g!key
         ,@(mapcar (lambda (k-a-b)
                     (destructuring-bind (key args &rest body) k-a-b
                       (if (or (eq key 'otherwise)
                               (eq key 't))
                           `(otherwise
                              (apply (lambda ,args ,@body) ,g!args))
                           `(,key
                              (apply (lambda ,args ,@body) ,g!rest-args)))))
                   key-args-body)
         (otherwise (error 'dlambda-key-not-found :key ,g!key))))))


;;; anaphoric macro
(defmacro anaphoric-bind-it (form &body body)
  `(let ((it ,form))
     (declare (ignorable it))
     ,@body))

(defmacro aprogn (&rest forms)
  (cond ((null forms)
         'nil)
        ((single? forms)
         (first forms))
        (t
         `(anaphoric-bind-it (first forms)
            ,@(rest forms)))))

(defmacro aif (test then else)
  `(anaphoric-bind-it ,test
     (if it ,then ,else)))

(defmacro awhen (test &body forms)
  `(anaphoric-bind-it ,test
     (when it ,@forms)))

(defmacro awhile (test &body body)
  `(do ((it ,test ,test))
       ((not it))
       (declare (ignorable it))
       ,@body))

(defmacro aand (&rest args)
  (cond ((null args) t)
        ((null (rest args)) (first args))
        (t `(aif ,(first args) (aand ,@(rest args))))))

(defmacro acond (&rest clauses)
  (if (null clauses)
      nil
      (destructuring-bind ((test &rest then) &rest rest-clauses) clauses
        `(aif ,test (progn ,@then) (acond ,@rest-clauses)))))

(defmacro alambda (args &body body)
  `(labels ((self ,args
              ,@body))
     #'self))

(defmacro alet (bindings &body body)
  `(let ((this nil) ,@bindings)
     (setq this ,@(last body))
     ,@(butlast body)
     (lambda (&rest params)
       (apply this params))))


;;; closure
;; closure collection
(defmacro define-closure-collection ((closure-name (&rest closure-args) (&rest closure-form))
                                     (defclosure-name (&rest defclosure-args) (&rest defclosure-form))
                                     &optional (meta-name 'meta-name) (meta-args 'meta-args))
  "Define META-CLOSURE, DEFINE-META-CLOSURE, META-DEFCLOSURE, and DEFINE-META-CLOSURE
   from given the closure and defclosure."
  (flet ((symb (&rest args)
           (values
             (intern
               (with-output-to-string (s)
                 (dolist (arg args) (princ arg s)))))))
    `(values
       (defmacro ,(symb "META-" closure-name) (,meta-args ,@closure-args)
         `(lambda ,,meta-args ,,closure-form))

       (defmacro ,(symb "DEFINE-META-" closure-name) (,meta-name ,meta-args ,@closure-args)
         `(defun ,,meta-name ,,meta-args ,,closure-form))

       (defmacro ,(symb "META-" defclosure-name) (,meta-args ,@defclosure-args)
         `(lambda ,,meta-args ,,defclosure-form))

       (defmacro ,(symb "DEFINE-META-" defclosure-name) (,meta-name ,meta-args ,@defclosure-args)
         `(defun ,,meta-name ,,meta-args ,,defclosure-form)))))

;; closure
(defmacro closure (bindings args &body body)
  `(let ,bindings
     (lambda ,args ,@body)))

(defmacro defclosure (name bindings args &body body)
  `(let ,bindings
     (defun ,name ,args ,@body)))

(define-closure-collection
  (closure (bindings args &body body) `(closure ,bindings ,args ,@body))
  (defclosure (name bindings args &body body) `(defclosure ,name ,bindings ,args ,@body)))

;; open-closure
(defmacro! open-closure (bindings &body key-args-body)
  (flet ((edit-bindings (x)
           (cond ((atom x) `(,x nil))
                 ((and (consp x) (null (cdr x))) `(,(first x) nil))
                 (t x))))
    (let ((bindings (cons '(this nil) (mapcar #'edit-bindings bindings))))
      `(let ,bindings
         (setq this
               (dlambda
                 ,@key-args-body
                 (:get (,g!var)
                  (case ,g!var
                    ,@(mapcar (lambda (binding)
                                `(,(first binding) ,(first binding)))
                              bindings)
                    (otherwise (error 'open-closure-variable-error
                                      :given-variable ,g!var
                                      :closure-variables ',(mapcar #'first bindings)))))
                 (:set (,g!var ,g!new)
                  (case ,g!var
                    ,@(mapcar (lambda (binding)
                                `(,(first binding) (setf ,(first binding) ,g!new)))
                              bindings)
                    (otherwise (error 'open-closure-variable-error
                                      :given-variable ,g!var
                                      :closure-variables ',(mapcar #'first bindings)))))))
         this))))

(defmacro! define-open-closure (name bindings &body key-args-body)
  (flet ((edit-bindings (x)
           (cond ((atom x) `(,x nil))
                 ((and (consp x) (null (cdr x))) `(,(first x) nil))
                 (t x))))
    (let ((bindings (cons '(this nil) (mapcar #'edit-bindings bindings))))
      `(let ,bindings
         (ddefun ,name
           ,@key-args-body
           (:get (,g!var)
            (case ,g!var
              ,@(mapcar (lambda (binding)
                          `(,(first binding) ,(first binding)))
                        bindings)
              (otherwise (error 'open-closure-variable-error
                                :given-variable ,g!var
                                :closure-variables ',(mapcar #'first bindings)))))
           (:set (,g!var ,g!new)
            (case ,g!var
              ,@(mapcar (lambda (binding)
                          `(,(first binding) (setf ,(first binding) ,g!new)))
                        bindings)
              (otherwise (error 'open-closure-variable-error
                                :given-variable ,g!var
                                :closure-variables ',(mapcar #'first bindings))))))
         (setq this #',name)
         ',name))))

(define-condition open-closure-variable-error (error)
  ((given-variable
     :initarg :given-variable
     :reader open-closure-variable-error-given-variable)
   (closure-variables
     :initarg :closure-variables
     :reader open-closure-variable-error-closure-variables))
  (:report
    (lambda (o s)
      (format s "~&The given variable~%~4t~a~%is not one of ~%~4t~{~a~^~}~%that are closure variables."
              (open-closure-variable-error-given-variable o)
              (open-closure-variable-error-closure-variables o)))))

(defmacro! open-closure-with-initialize (bindings &body key-args-body)
  `(let ((,g!open-closure (open-closure ,bindings ,@key-args-body)))
     (funcall ,g!open-closure :initialize)
     ,g!open-closure))

(declaim (inline get-open-closure))
(defun get-open-closure (key open-closure)
  (funcall open-closure :get key))
(defsetf get-open-closure (key open-closure) (new)
         `(progn
            (funcall ,open-closure :set ,key ,new)
            ,new))

(defmacro! with-open-closure (bindings o!open-closure &body body)
  (let ((bindings (mapcar (lambda (binding)
                            (cond ((atom binding)
                                   `(,binding ,binding))
                                  ((single? binding)
                                   `(,(first binding) ,(first binding)))
                                  ((n-list? 2 binding)
                                   binding)
                                  (t
                                   (error 'invalid-let-bindings
                                          :bindings :bindings
                                          :context 'with-open-closure))))
                          bindings)))
    `(let ,(mapcar (lambda (binding)
                     `(,(first binding)
                        (get-open-closure ',(second binding) ,g!open-closure)))
                   bindings)
       ,@body)))

(defmacro! with-open-closure* (bindings o!open-closure &body body)
  (let ((bindings (mapcar (lambda (binding)
                            (cond ((atom binding)
                                   `(,binding ,binding))
                                  ((single? binding)
                                   `(,(first binding) ,(first binding)))
                                  ((n-list? 2 binding)
                                   binding)
                                  (t
                                   (error 'invalid-let-bindings
                                          :bindings :bindings
                                          :context 'with-open-closure))))
                          bindings)))
    `(symbol-macrolet
       ,(mapcar (lambda (binding)
                  `(,(first binding)
                     (get-open-closure ',(second binding) ,g!open-closure)))
                bindings)
       ,@body)))

(define-closure-collection
  (open-closure
    (bindings &body key-args-body)
    `(open-closure ,bindings ,@key-args-body))
  (define-open-closure
    (name bindings &body key-args-body)
    `(define-open-closure ,name ,bindings ,@key-args-body)))


;;; lazy evaluation
(defstruct (promise (:constructor make-promise (thunk))
                    (:predicate promise?)
                    (:print-object
                      (lambda (o s)
                        (if (promise-forced? o)
                            (format s "#<PROMISE-forced cache: ~s>"
                                    (promise-cache o))
                            (format s "#<PROMISE-unforced>")))))
  (thunk thunk)
  (forced? nil)
  (cache nil))

(defmacro delay (&rest body)
  `(make-promise (lambda () ,@body)))

(defun force (promise)
  (if (promise? promise)
      (with-slots (thunk forced? cache) promise
        (unless forced?
          (setf forced? t
                cache (funcall thunk)))
        cache)
      (error 'type-error
             :datum promise
             :expected-type 'promise)))

;;; lazy-stream
(defmacro lcons (se1 se2)
  `(delay (cons ,se1 ,se2)))

(defun lcar (lcons)
  (car (force lcons)))

(defun lcdr (lcons)
  (cdr (force lcons)))

(defun lnull (object)
  (and (promise? object)
       (null (force object))))

(defmacro lstream (&rest args)
  (if (null args)
      `(delay '())
      `(lcons ,(car args) (lstream ,@(cdr args)))))

(defun lref (n lstream)
  (declare (optimize speed) (type fixnum n))
  (cond ((zerop n)
         (lcar lstream))
        ((plusp n)
         (lref (1- n) (lcdr lstream)))
        ((minusp n)
         (error 'index-error
                :context 'lref))))

(defun ltake (n lstream)
  (declare (optimize speed) (type fixnum n))
  (cond ((zerop n)
         (delay '()))
        ((plusp n)
         (lcons (lcar lstream)
                (ltake (1- n) (lcdr lstream))))
        ((minusp n)
         (error 'index-error
                :context 'ltake))))

(defun ldrop (n lstream)
  (declare (optimize speed) (type fixnum n))
  (cond ((zerop n)
         lstream)
        ((plusp n)
         (ldrop (1- n) (lcdr lstream)))
        ((minusp n)
         (error 'index-error
                :context 'ldrop))))

(defun lreverse (lstream)
  (labels ((rec (ls acc)
             (declare (optimize speed))
             (if (lnull ls)
                 acc
                 (rec (lcdr ls) (lcons (lcar ls) acc)))))
    (rec lstream (delay '()))))

(defun lappend (&rest lss)
  (cond ((null lss)
         (delay '()))
        ((lnull (lcar lss))
         (apply #'append lss))
        (t
         (lcons (lcar (lcar lss))
                (apply #'append (lcdr (lcar lss)) (lcdr lss))))))

(defun lmember (item lstream &key (test #'eql))
  (declare (optimize speed) (type function test))
  (cond ((lnull lstream)
         nil)
        ((funcall test item (lcar lstream))
         lstream)
        (t
         (lmember item (lcdr lstream) :test test))))

(defun lmap (function lstream &rest more-lstreams)
  (let ((lstreams (cons lstream more-lstreams)))
    (if (some #'lnull lstreams)
        (delay '())
        (lcons (apply function (mapcar #'lcar lstreams))
               (apply #'lmap function (mapcar #'lcdr lstreams))))))


;;; read macros
(defun sharp-backquote-reader (stream sub-char numarg)
  (declare (ignore sub-char))
  (unless numarg (setq numarg 1))
  `(lambda ,(loop :for i :from 1 :to numarg
                  :collect (symbolicate 'a i))
     ,(funcall
        (get-macro-character #\`) stream nil)))

(defun sharp-backquote-toggle ()
  (set-dispatch-macro-character
    #\# #\` #'sharp-backquote-reader))
(sharp-backquote-toggle)


;;; conditions
(define-condition index-error (error)
  ((context :initarg :context
            :reader index-error-context))
  (:report
    (lambda (o s)
      (format s "~&Out of index at ~a."
              (index-error-context o)))))

(define-condition invalid-let-bindings (error)
  ((bindings :initarg :bindings
             :reader invalid-let-bindings-bindings)
   (context :initarg :context
            :reader invalid-let-bindings-context))
  (:report
    (lambda (o s)
      (format s "~&The let bindings form ~%~6t~a~%in~%~6t~a~%is invalid.~%"
              (invalid-let-bindings-bindings o)
              (invalid-let-bindings-context o)))))

(defpackage data-frame
  (:nicknames :df)
  (:export
    ;; core
    #:df
    #:get-df
    #:print-df
    #:head-df
    #:add-new-row
    #:add-new-row!
    #:add-new-col
    #:add-new-col!
    #:map-cols

    ;; io
    #:*csv-separator*
    #:csv->df
    #:df->csv
    #:read-csv-stream
    #:read-csv-file
    #:write-csv-stream
    #:write-csv-file

    ;; stats
    #:sum
    #:count-up
    #:maximum
    #:minimum
    #:median
    #:first-quartile #:third-quartile
    #:mean
    #:variance
    #:standard-deviation
    #:unbiased-variance
    #:standard-error
    #:covariance
    #:correlation-coefficient
    )
  )

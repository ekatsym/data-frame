(defpackage data-frame.io
  (:nicknames :df.io)
  (:use :cl)
  (:import-from :df
                #:df
                #:*csv-separator*
                #:csv->df
                #:df->csv
                #:read-csv-stream
                #:read-csv-file
                #:write-csv-stream
                #:write-csv-file)
  (:import-from :df.util
                #:split
                #:shuffle
                #:read-stream
                #:read-file)
  (:import-from :df.core
                #:df-headers
                #:get-df))
(in-package df.io)


(defparameter *csv-separator* #\,)

;;; input
(defun csv->rows (csv)
  (mapcar (lambda (row)
            (mapcar (lambda (cell)
                      (let* ((str (coerce cell 'string))
                             (read-data (read-from-string str nil nil)))
                        (if (numberp read-data)
                            read-data
                            str)))
                    (split *csv-separator* row)))
          (split #\newline (coerce csv 'list))))

(defun rows->df (rows)
  (labels ((transpose (lsts acc)
             (if (every #'null lsts)
                 (nreverse acc)
                 (transpose (mapcar #'rest lsts) (cons (mapcar #'first lsts) acc)))))
    (let ((headers (first rows))
          (contents (transpose (rest rows) '())))
      (apply #'df (shuffle headers contents)))))

(defun csv->df (csv)
  (rows->df (csv->rows csv)))

(defun read-csv-stream (stream)
  (read-stream stream))

(defun read-csv-file (filespec)
  (read-file filespec))


;;; output
(defun df->rows (df)
  (labels ((transpose (lsts acc)
             (declare (optimize (speed 3)))
             (if (every #'null lsts)
                 (nreverse acc)
                 (transpose (mapcar #'rest lsts)
                            (cons (mapcar #'first lsts) acc)))))
    (cons (df-headers df)
          (transpose (mapcar (lambda (header)
                               (get-df df header))
                             (df-headers df))
                     '()))))

(defun rows->csv (rows)
  (with-output-to-string (s)
    (format s (concatenate
                'string
                "~{~{~a~^" (string *csv-separator*) "~}~^~%~}")
            rows)))

(defun df->csv (df)
  (rows->csv (df->rows df)))

(defun write-csv-stream (df &optional (stream *standard-output*))
  (format stream (df->csv df)))

(defun write-csv-file (df filespec)
  (with-open-file (s filespec
                     :direction :output
                     :if-does-not-exist :create
                     :if-exists :error)
    (format s (df->csv df))))

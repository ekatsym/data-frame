(defpackage data-frame.stats
  (:nicknames :df.stats)
  (:use :cl)
  (:import-from :df
                #:df
                #:get-df
                #:sum
                #:count-up
                #:maximum #:minimum #:median #:first-quartile #:third-quartile
                #:mean #:variance #:standard-deviation
                #:unbiased-variance #:standard-error
                #:covariance #:correlation-coefficient
                )
  )
(in-package :df.stats)


(defun sum (df header)
  (df.util:sum (get-df df header)))

(defun count-up (df header)
  (count-if-not (lambda (x) (member x '("None" "NaN" "NaT")))
                (get-df df header)))

(defun maximum (df header)
  (df.util:maximum (get-df df header)))

(defun minimum (df header)
  (df.util:minimum (get-df df header)))

(defun median (df header)
  (df.util:median (get-df df header)))

(defun first-quartile (df header)
  (df.util:first-quartile (get-df df header)))

(defun third-quartile (df header)
  (df.util:third-quartile (get-df df header)))

(defun mean (df header)
  (df.util:mean (get-df df header)))

(defun variance (df header)
  (df.util:variance (get-df df header)))

(defun standard-deviation (df header)
  (df.util:standard-deviation (get-df df header)))

(defun unbiased-variance (df header)
  (df.util:unbiased-variance (get-df df header)))

(defun standard-error (df header)
  (df.util:standard-error (get-df df header)))

(defun covariance (df header1 header2)
  (df.util:covariance
    (mapcar #'cons
            (get-df df header1)
            (get-df df header2))))

(defun correlation-coefficient (df header1 header2)
  (df.util:correlation-coefficient
    (mapcar #'cons
            (get-df df header1)
            (get-df df header2))))

(defsystem "data-frame"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :components ((:module "src"
                :serial t
                :components
                ((:file "util")
                 (:file "package")
                 (:file "core")
                 (:file "io")
                 (:file "stats")
                 )))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "data-frame/tests"))))

(defsystem "data-frame/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("data-frame"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for data-frame"

  :perform (test-op (op c) (symbol-call :rove :run c)))

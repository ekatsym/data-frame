(defpackage data-frame/tests/main
  (:use :cl
        :data-frame
        :rove))
(in-package :data-frame/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :data-frame)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
